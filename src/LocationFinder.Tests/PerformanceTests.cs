﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using CsvHelper;
using NUnit.Framework;
using Roamler.LocationFinder.App.Domain;
using Shouldly;

namespace Roamler.LocationFinder.Tests
{
    [Category("Performance")]
    [TestFixture]
    public class PerformanceTests
    {
        [TestCase(10)]
        [TestCase(100)]
        [TestCase(1000)]
        public void GeneratedDataSearchPerformance(int neighboursCount)
        {
            var timer = new Stopwatch();
            var locations = GetGeneratedData();
            var targetLocation = new Location(locations[locations.Count / 2].Latitude, locations[locations.Count / 2 + 1].Longitude);
            var searchEngine = SearchEngine.Build(locations);

            timer.Reset();
            timer.Start();
            var result = searchEngine.GetNearbyLocations(targetLocation, 30000, neighboursCount);
            timer.Stop();
            var searchTime = timer.Elapsed;
            Console.WriteLine($"Search time: {searchTime}");

            searchTime.ShouldBeLessThan(TimeSpan.FromMilliseconds(300));
        }

        [Test]
        public void SampleDataSearchPerformance()
        {
            var timer = new Stopwatch();
            var locations = GetSampleData();
            var neighboursCount = 100;
            var targetLocation = new Location(locations[1232].Latitude, locations[1233].Longitude);
            var searchEngine = SearchEngine.Build(locations);

            timer.Start();
            searchEngine.GetNearbyLocations(targetLocation, 3000, neighboursCount);
            timer.Stop();
            var searchTime = timer.Elapsed;
            Console.WriteLine($"Search time: {searchTime}");

            searchTime.ShouldBeLessThan(TimeSpan.FromMilliseconds(300));
        }

        [Test]
        public void TreeBuildingPerformance()
        {
            var timer = new Stopwatch();
            var locations = GetGeneratedData();

            timer.Start();
            var result = SearchEngine.Build(locations);
            timer.Stop();
            var treeBuildingTime = timer.Elapsed;
            Console.WriteLine($"Build time: {treeBuildingTime}");

            treeBuildingTime.ShouldBeLessThan(TimeSpan.FromMinutes(1));
        }

        private static List<Location> GetSampleData()
        {
            var locationsResourceName = "Roamler.LocationFinder.Tests.SampleData.locations.csv";
            var assembly = Assembly.GetCallingAssembly();
            using (var localDataStream = assembly.GetManifestResourceStream(locationsResourceName))
            {
                using (TextReader reader = new StreamReader(localDataStream))
                {
                    using (var csv = new CsvReader(reader))
                    {
                        var locations = csv.GetRecords<Location>().ToList();
                        return locations;
                    }
                }
            }
        }

        private static List<Location> GetGeneratedData()
        {
            var locations = GetSampleData();
            var additionalLocations = new List<Location>();

            foreach (var location in locations)
            {
                var lat = location.Latitude;
                var lon = location.Longitude;
                var address = location.Address;
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000001));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000002));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000003));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000004));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000005));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000006));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000007));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000008));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000009));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000010));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000011));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000012));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000013));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000014));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000015));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000016));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000017));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000018));
                additionalLocations.AddRange(GenerateNearbyLocations(lat, lon, address, 0.000019));
            }

            locations.AddRange(additionalLocations);

            return locations;
        }

        private static List<Location> GenerateNearbyLocations(double lat, double lon, string address, double offset)
        {
            var locations = new List<Location>
            {
                new Location(lat, lon - offset, address),
                new Location(lat - offset, lon - offset, address),
                new Location(lat + offset, lon, address),
                new Location(lat, lon + offset, address),
                new Location(lat + offset, lon + offset, address)
            };

            return locations;
        }
    }
}