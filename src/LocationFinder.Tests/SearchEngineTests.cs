using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Roamler.LocationFinder.App.Domain;
using Shouldly;

namespace Roamler.LocationFinder.Tests
{
    public class SearchEngineTests
    {
        [Test]
        public void WhenMaxResultsGreaterThanTotalNumberOfRecords_ShouldReturnAll()
        {
            var testLocations = new List<Location>
            {
                GenerateLocation(0.0, 0.0),
                GenerateLocation(0.0, 1.0),
                GenerateLocation(0.0, -1.0)
            };
            var target = SearchEngine.Build(testLocations);

            var actual = target.GetNearbyLocations(GenerateLocation(-0.1, -0.1), int.MaxValue, 5);

            actual.Count.ShouldBe(3);
        }

        [Test]
        public void ShouldCalculateDistance()
        {
            var testLocations = new List<Location>
            {
                GenerateLocation(0.0, 0.0),
                GenerateLocation(0.0, 1.0),
                GenerateLocation(0.0, -1.0)
            };
            var target = SearchEngine.Build(testLocations);

            var actual = target.GetNearbyLocations(GenerateLocation(-0.1, -0.1), int.MaxValue, 5);

            actual.ShouldAllBe(r => r.DistanceMeters > 0.0);
        }

        [Test]
        public void ShouldCalculateCorrectDistance()
        {
            var testLocations = new List<Location>
            {
                GenerateLocation(0.0, 0.0),
                GenerateLocation(0.0, 1.0),
                GenerateLocation(0.0, -1.0)
            };
            var targetLocation = GenerateLocation(-0.1, -0.1);
            var target = SearchEngine.Build(testLocations);

            var actual = target.GetNearbyLocations(targetLocation, int.MaxValue, 5);

            foreach (var searchResult in actual)
            {
                searchResult.DistanceMeters.ShouldBe(targetLocation.CalculateDistance(searchResult.Location), 0.0000001);
            }
        }

        [Test]
        public void WhenMaxResultsLesserThanTotalNumberOfRecords_ShouldReturnMaxResults()
        {
            var testLocations = new List<Location>
            {
                GenerateLocation(0.0, 0.0),
                GenerateLocation(0.0, 1.0),
                GenerateLocation(0.0, -1.0)
            };
            var target = SearchEngine.Build(testLocations);

            var actual = target.GetNearbyLocations(GenerateLocation(-0.1, -0.1), int.MaxValue, 2);

            actual.Count.ShouldBe(2);
        }

        [Test]
        public void WhenSomeNeighboursAreFurtherThanMaxDistance_ShouldReturnOnlyClosest()
        {
            var testLocations = new List<Location>
            {
                GenerateLocation(0.0, 0.1),
                GenerateLocation(0.0, 10.0),
                GenerateLocation(0.0, -0.1),
                GenerateLocation(0.0, -10.0)
            };
            var target = SearchEngine.Build(testLocations);

            var actual = target.GetNearbyLocations(GenerateLocation(0.0, 0.0), 20000, 4);

            actual.Count.ShouldBe(2);
            actual.ShouldContain(r => r.Location == testLocations[0]);
            actual.ShouldContain(r => r.Location == testLocations[2]);
        }

        [Test]
        public void WhenRequestedOneNeighbour_ShouldReturnOnlyClosest()
        {
            var testLocations = new List<Location>
            {
                GenerateLocation(-1.0, -1.0),
                GenerateLocation(0.0, 1.0),
                GenerateLocation(1.0, 1.0)
            };

            var target = SearchEngine.Build(testLocations);

            var actual = target.GetNearbyLocations(GenerateLocation(0.0, 0.0), int.MaxValue, 1);

            actual.Count.ShouldBe(1);
            actual[0].Location.ShouldBe(testLocations[1]);
        }

        [Test]
        public void ShouldReturnCorrectNeighbours()
        {
            var targetLocation = GenerateLocation(53.0296858, -2.1978525);
            var testLocations = new List<Location>
            {
                GenerateLocation(53.031305, -2.192611), // distance: 394.026736371901
                GenerateLocation(53.025075, -2.1929713), // distance: 607.76986164497
                GenerateLocation(53.0199337, -2.2030268), // distance: 1138.20947676277
                GenerateLocation(53.028821, -2.210284), // distance: 836.83981074015
                GenerateLocation(53.0278233, -2.2102642), // distance: 855.428330272695
                GenerateLocation(53.026124, -2.182778), // distance: 1083.07055553513
                GenerateLocation(53.04608, -2.19516), // distance: 1831.73089624958
                GenerateLocation(53.02321626, -2.180201411), // distance: 1382.32506626582
                GenerateLocation(53.0476362, -2.2037537) // distance: 2034.51689849734
            };
            var target = SearchEngine.Build(testLocations);

            var actual = target.GetNearbyLocations(targetLocation, int.MaxValue, 4);

            actual.Count.ShouldBe(4);
            actual[0].Location.ShouldBe(testLocations[0]);
            actual[1].Location.ShouldBe(testLocations[1]);
            actual[2].Location.ShouldBe(testLocations[3]);
            actual[3].Location.ShouldBe(testLocations[4]);
        }

        [Test]
        public void ShouldSortNeighboursByDistance()
        {
            var testLocations = new List<Location>
            {
                GenerateLocation(53.031305, -2.192611),
                GenerateLocation(53.025075, -2.1929713),
                GenerateLocation(53.0199337, -2.2030268),
                GenerateLocation(53.028821, -2.210284),
                GenerateLocation(53.0278233, -2.2102642),
                GenerateLocation(53.026124, -2.182778)
            };
            var target = SearchEngine.Build(testLocations);

            var actual = target.GetNearbyLocations(GenerateLocation(0.0, 0.0), int.MaxValue, int.MaxValue);

            actual.Count.ShouldBe(6);
            actual.Select(a => a.DistanceMeters).ShouldBeInOrder(SortDirection.Ascending);
        }

        [Test]
        public void WhenLocationIsAdded_ShouldAppearInSearch()
        {
            var testLocations = new List<Location>
            {
                GenerateLocation(0.0, 0.0),
                GenerateLocation(0.0, 1.0),
                GenerateLocation(0.0, -1.0)
            };
            var additionalLocation = GenerateLocation(2.0, 2.0);
            var target = SearchEngine.Build(testLocations);

            target.AddLocation(additionalLocation);
            var actual = target.GetNearbyLocations(GenerateLocation(-0.1, -0.1), int.MaxValue, 5);

            actual.Count.ShouldBe(4);
            actual.ShouldContain(r => r.Location == additionalLocation);
        }

        private static Location GenerateLocation(double latitude, double longitude)
        {
            return new Location(latitude, longitude, $"Test Location {latitude}, {longitude}");
        }
    }
}