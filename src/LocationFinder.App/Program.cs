﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Roamler.LocationFinder.App.Domain;

namespace Roamler.LocationFinder.App
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var locations = new List<Location>();
            var searchEngine = Benchmark(() => new SearchEngine(locations), $"{nameof(SearchEngine)} creation for {locations.Count} locations");
            var neighboursCount = 100;
            var testLocation = locations[1];

            var neighbours = Benchmark(
                () => searchEngine.GetNearbyLocations(testLocation, 3000, neighboursCount),
                $"Find {neighboursCount} nearest neighbours from {locations.Count} locations");

            Console.WriteLine($"Location {testLocation}:");
            Console.WriteLine($"{neighboursCount} neighbours: {string.Join(Environment.NewLine, neighbours.Select(n => $"{n.Location}: {n.DistanceMeters}"))}");
            Console.WriteLine("Done!");
            Console.ReadKey();
        }

        private static T Benchmark<T>(Func<T> call, string name)
        {
            var timer = new Stopwatch();
            timer.Start();
            var result = call();
            timer.Stop();

            Console.WriteLine($"Call '{name}' took {timer.Elapsed}");

            return result;
        }
    }
}