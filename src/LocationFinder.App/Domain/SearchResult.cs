﻿namespace Roamler.LocationFinder.App.Domain
{
    public class SearchResult
    {
        public Location Location { get; set; }
        public double DistanceMeters { get; set; }
    }
}