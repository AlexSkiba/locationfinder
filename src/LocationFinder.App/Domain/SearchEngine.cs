﻿using System;
using System.Collections.Generic;
using System.Linq;
using KdTree;

namespace Roamler.LocationFinder.App.Domain
{
    public class SearchEngine
    {
        private readonly KdTree<double, Location> _searchTree;

        public static SearchEngine Build(IEnumerable<Location> initialLocations)
        {
            if (initialLocations == null)
            {
                throw new ArgumentNullException();
            }

            var engine = new SearchEngine();
            foreach (var location in initialLocations)
            {
                engine.AddLocation(location);
            }

            return engine;
        }

        public SearchEngine()
        {
            _searchTree = new KdTree<double, Location>(2, new LocationMath());
        }

        public SearchEngine(IEnumerable<Location> initialLocations)
        {
            _searchTree = new KdTree<double, Location>(2, new LocationMath());
            if (initialLocations == null)
            {
                throw new ArgumentNullException();
            }

            var engine = new SearchEngine();
            foreach (var location in initialLocations)
            {
                _searchTree.Add(new[] {location.Latitude, location.Longitude}, location);
            }
        }

        public List<SearchResult> GetNearbyLocations(Location location, int maxDistanceMeters, int maxResults)
        {
            var neighbours = _searchTree.GetNearestNeighbours(new[] {location.Latitude, location.Longitude}, maxResults);

            return neighbours
                .Select(n => new SearchResult {Location = n.Value, DistanceMeters = n.Value.CalculateDistance(location)})
                .Where(r => r.DistanceMeters <= maxDistanceMeters)
                .OrderBy(r => r.DistanceMeters)
                .ToList();
        }

        public void AddLocation(Location newLocation)
        {
            _searchTree.Add(new[] {newLocation.Latitude, newLocation.Longitude}, newLocation);
        }
    }
}