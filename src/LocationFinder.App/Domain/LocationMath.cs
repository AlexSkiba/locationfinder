﻿using KdTree.Math;

namespace Roamler.LocationFinder.App.Domain
{
    internal class LocationMath : DoubleMath
    {
        public override double DistanceSquaredBetweenPoints(double[] a, double[] b)
        {
            var locationA = new Location(a[0], a[1]);
            var locationB = new Location(b[0], b[1]);
            return locationA.CalculateDistance(locationB);
        }
    }
}